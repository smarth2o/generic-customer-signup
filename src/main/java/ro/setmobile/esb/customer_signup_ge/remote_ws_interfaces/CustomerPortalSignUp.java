package ro.setmobile.esb.customer_signup_ge.remote_ws_interfaces;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/CustomerPortalSignup")
public interface CustomerPortalSignUp {
	
	@POST
	@Path("/customerPortalSignup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String customerPortalSignUpGe(String json);
}
